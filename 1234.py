while True:
    try:      
        test = input()

        j = 0
        stringNova = ''
        for i in range(len(test)):
            if ord(test[i]) == 32:
                stringNova += ' '
            else:
                if j % 2 == 0:
                    if ord(test[i]) >= 97 and ord(test[i]) <= 122 :
                        stringNova += chr(ord(test[i]) - 32)
                    elif ord(test[i]) >= 65 and ord(test[i]) <= 90:
                        stringNova += chr(ord(test[i]))
                else:
                    if ord(test[i]) >= 65 and ord(test[i]) <= 90:
                        stringNova += chr(ord(test[i]) + 32)
                    elif ord(test[i]) >= 97 and ord(test[i]) <= 122:
                        stringNova += chr(ord(test[i]))
                j += 1

        print(stringNova)
    except EOFError:
        break
