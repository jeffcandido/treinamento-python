N = input()
Li = list(map(int,input().split()))

dois, tres, quatro, cinco = 0, 0, 0, 0

for i in range(int(N)):
    if Li[i] % 5 == 0:
        cinco += 1
    if  Li[i] % 4 == 0:
        quatro += 1
        dois += 1
    else:
        if  Li[i] % 2 == 0:
            dois += 1
    if  Li[i] % 3 == 0:
        tres += 1

print(str(dois) + " Multiplo(s) de 2")
print(str(tres) + " Multiplo(s) de 3")
print(str(quatro) + " Multiplo(s) de 4")
print(str(cinco) + " Multiplo(s) de 5")
